-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2019 at 09:08 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `extranet`
--

-- --------------------------------------------------------

--
-- Table structure for table `usuarios_old`
--

CREATE TABLE `usuarios_old` (
  `id_usuario_old` int(11) NOT NULL,
  `rif_extranet` varchar(10) NOT NULL,
  `razon_social` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuarios_old`
--

INSERT INTO `usuarios_old` (`id_usuario_old`, `rif_extranet`, `razon_social`) VALUES
(1, 'V200959627', 'ELIAS CARACAS (EXTRANET)');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `usuarios_old`
--
ALTER TABLE `usuarios_old`
  ADD PRIMARY KEY (`id_usuario_old`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `usuarios_old`
--
ALTER TABLE `usuarios_old`
  MODIFY `id_usuario_old` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
