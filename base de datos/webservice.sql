-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2019 at 09:08 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webservice`
--

-- --------------------------------------------------------

--
-- Table structure for table `contribuyentes`
--

CREATE TABLE `contribuyentes` (
  `id_contribuyente` int(11) NOT NULL,
  `rif_servicio` varchar(10) NOT NULL,
  `razon_social` varchar(100) NOT NULL,
  `telefono` bigint(11) NOT NULL,
  `nombres` varchar(50) DEFAULT NULL,
  `apellidos` varchar(50) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `estatus` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contribuyentes`
--

INSERT INTO `contribuyentes` (`id_contribuyente`, `rif_servicio`, `razon_social`, `telefono`, `nombres`, `apellidos`, `correo`, `estatus`) VALUES
(1, 'J000959627', 'ELIAS CARACAS SERVICIO ', 4123737328, NULL, NULL, NULL, 1),
(2, 'V100959627', 'prueba1', 4123737328, '', '', '', 2),
(3, 'V500959627', 'prueba1', 4123737328, '', '', '', 2),
(4, 'V300959627', 'prueba1', 0, '', '', '', 2),
(5, 'V800959627', 'prueba1', 0, '', '', '', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contribuyentes`
--
ALTER TABLE `contribuyentes`
  ADD PRIMARY KEY (`id_contribuyente`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contribuyentes`
--
ALTER TABLE `contribuyentes`
  MODIFY `id_contribuyente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
