<?php

namespace app\controllers;

class ApiController extends \yii\web\Controller
{
    // http://localhost/WSYii2Soap/web/api/index

    public function actionIndex()
    {
        echo "hola";exit;
        return $this->render('index');
    }

    // https://github.com/mohorev/yii2-soap-server

    public function actions()
    {
        return [
            'hello' => 'mongosoft\soapserver\Action',
            'rif' => 'mongosoft\soapserver\Action',
            'suma' => 'mongosoft\soapserver\Action',
        ];
    }

    /**
     * @param string $name
     * @return string
     * @soap
     */
    public function getHello($name)
    {
        return 'Hola: ' . $name;
    }
    /**
     * @param string $rif
     * @return string
     * @soap
     */
    public function getRif($rif)
    {
        return 'rif'.$rif;
    }
    /**
     * @param int $num1
     * @param int $num2
     * @param int $result
     * @return int
     * @soap
     */
    public function getSuma($num1,$num2)  
    {
        $result = $num1 + $num2;
        
        return $result;
    }

}
