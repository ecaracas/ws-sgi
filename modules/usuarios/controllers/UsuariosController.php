<?php

namespace app\modules\usuarios\controllers;

use app\modules\usuarios\models\UsuariosOld;
use app\modules\usuarios\models\Usuarios;
use yii\helpers\ArrayHelper;
// use yii\console\Exception;
use mongosoft\soapclient\Exception;
use app\modules\usuarios\models\Contribuyentes;
use app\modules\usuarios\models\Validaciones;



class UsuariosController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $rif = "J200959627";
        $razonsocial = "";
        $nombres = "otro nombre";
        $apellidos = "uia quibusdam eligendi veritatis";
        $telefono = "(0424)292-61-94";
        $correo = "otro@correo.com";   
        //al emplear el metodo getrif se realizan todas las validaciones para el rif automaticamente
        $rifVal = $this->getRif($rif);
        $datosContribuyente = explode('||',$rifVal);
        $estatusContribuyente = $datosContribuyente[0];
        $rifContribuyente = $datosContribuyente[1];
        $rsContribuyente = $datosContribuyente[2];
        $nombresContribuyente = $datosContribuyente[3];
        $apellidosContribuyente = $datosContribuyente[4];
        $telefonoContribuyente = $datosContribuyente[5];
        $correoContribuyente = $datosContribuyente[6];
        // print_r($estatusContribuyente);die;
        //dependiendo del response del getRif procedemos a hacer set de los datos del contribuyente
        switch ($estatusContribuyente) {
            case '0':
            //estatus = 0 : si el rif no existe en las BD se procede a realizar un insert en la base de datos del servicio
                $nuevoContribuyente = new Contribuyentes();
                $nuevoContribuyente->rif_servicio =  $rif;
                $nuevoContribuyente->razon_social =  $razonsocial;
                $nuevoContribuyente->telefono = $telefono;
                $nuevoContribuyente->estatus = $nuevoContribuyente->getEstatus($nombres,$apellidos,$correo);
                if ($nuevoContribuyente->telefono) { 
                    if ($nuevoContribuyente->validate()) {
                        $nuevoContribuyente->save();
                        if ($nuevoContribuyente->save()) {
                            return '200'; //se crea un nuevo registro para el contribuyente que no fue encontrado en ninguna BD
                        }else{
                            return '102'; // ocurrio un error al guardar en la base de datos
                        }
                    }else {
                        return '101'; // no procesado error en validaciones
                    }
                }else{
                    return '103'; // telefono no puede estar en blanco
                }
                    break;
            case '1':
            //estatus = 1 : tiene toda la informacion pero la actualiza
                $actualizado = Contribuyentes::find()->where(['rif_servicio'=>$rifContribuyente])->one();
                $actualizado->telefono = $telefono;
                $actualizado->estatus = $actualizado->getEstatus($nombres,$apellidos,$correo);
                if ($actualizado->telefono) { 
                    if ($actualizado->validate()) {
                        $actualizado->save();
                        if ($actualizado->save()) {
                            return '200'; //se crea un nuevo registro para el contribuyente 
                        }else{
                            return '102'; // ocurrio un error al guardar en la base de datos
                        }
                    }else {
                        print_r($actualizado->getErrors());
                        return '101'; // no procesado error en validaciones
                    }
                }else{
                    return '103'; // telefono no puede estar en blanco
                }
                break;
            case '2':
            //estatus = 2 : le falta toda la informacion y realiza algun cambio
                $actualizado = Contribuyentes::find()->where(['rif_servicio'=>$rifContribuyente])->one();
                $actualizado->telefono = $telefono;
                $actualizado->estatus = $actualizado->getEstatus($nombres,$apellidos,$correo);
                if ($actualizado->telefono) { 
                    if ($actualizado->validate()) {
                        $actualizado->save();
                        if ($actualizado->save()) {
                            return '200'; //se crea un nuevo registro para el contribuyente 
                        }else{
                            return '102'; // ocurrio un error al guardar en la base de datos
                        }
                    }else {
                        return '101'; // no procesado error en validaciones
                    }
                }else{
                    return '103'; // telefono no puede estar en blanco
                }
                break;
            case '3':
            //estatus = 3 : le faltan dos datos
                $actualizado = Contribuyentes::find()->where(['rif_servicio'=>$rifContribuyente])->one();
                $actualizado->telefono = $telefono;
                $actualizado->estatus = $actualizado->getEstatus($nombres,$apellidos,$correo);
                if ($actualizado->telefono) { 
                    if ($actualizado->validate()) {
                        $actualizado->save();
                        if ($actualizado->save()) {
                            return '200'; //se crea un nuevo registro para el contribuyente 
                        }else{
                            return '102'; // ocurrio un error al guardar en la base de datos
                        }
                    }else {
                        return '101'; // no procesado error en validaciones
                    }
                }else{
                    return '103'; // telefono no puede estar en blanco
                }
                break;
            case '4':
            //estatus = 4 : le falta al menos un dato
                $actualizado = Contribuyentes::find()->where(['rif_servicio'=>$rifContribuyente])->one();
                $actualizado->telefono = $telefono;
                $actualizado->estatus = $actualizado->getEstatus($nombres,$apellidos,$correo);
                if ($actualizado->telefono) { 
                    if ($actualizado->validate()) {
                        $actualizado->save();
                        if ($actualizado->save()) {
                            return '200'; //se crea un nuevo registro para el contribuyente 
                        }else{
                            return '102'; // ocurrio un error al guardar en la base de datos
                        }
                    }else {
                        return '101'; // no procesado error en validaciones
                    }
                }else{
                    return '103'; // telefono no puede estar en blanco
                }
                break;
            case '6':
            //estatus = 6 : errores en validacion
                return "101";
                break;
            default:
            // no realizo ningun cambio
                return "100"; 
                break;
        }
    }

    public function actions()
    {
        return [
            'servicios' => [
                'class'=>'mongosoft\soapserver\Action',
            ]
        ];
    }
        /**
         * @param string $rif
         * @param string $razonsocial
         * @param string $telefono
         * @param string $nombres
         * @param string $apellidos
         * @param string $correo
         * @param string $estatus
         * @return string
         * @soap
         */
        public function setRif($rif,$razonsocial,$telefono,$nombres,$apellidos,$correo)
        {
            if(!$rif){
                throw new Exception('falta uno de los parametros');
            }

            try {
            //se emplea el metodo getrif para ejecutar todas las validaciones automaticamente
                $rifVal = $this->getRif($rif);
                $datosContribuyente = explode('||',$rifVal);
                $estatusContribuyente = $datosContribuyente[0];
                $rifContribuyente = $datosContribuyente[1];
                $rsContribuyente = $datosContribuyente[2];
                $nombresContribuyente = $datosContribuyente[3];
                $apellidosContribuyente = $datosContribuyente[4];
                $telefonoContribuyente = $datosContribuyente[5];
                $correoContribuyente = $datosContribuyente[6];
            //dependiendo del estatus del contribuyente procedemos a hacer set de los datos del contribuyente
                switch ($estatusContribuyente) {
                    case '0':
                    //estatus = 0 : si el rif no existe en las BD se procede a realizar un insert en la base de datos del servicio
                        $nuevoContribuyente = new Contribuyentes();
                        $nuevoContribuyente->rif_servicio =  $rif;
                        $nuevoContribuyente->razon_social =  $razonsocial;
                        $nuevoContribuyente->telefono = $telefono;
                        $nuevoContribuyente->estatus = $nuevoContribuyente->getEstatus($nombres,$apellidos,$correo);
                        if ($nuevoContribuyente->telefono) { 
                            if ($nuevoContribuyente->validate()) {
                                $nuevoContribuyente->save();
                                if ($nuevoContribuyente->save()) {
                                    return '200'; //se crea un nuevo registro para el contribuyente que no fue encontrado en ninguna BD
                                }else{
                                    return '102'; // ocurrio un error al guardar en la base de datos
                                }
                            }else {
                                return '101'; // no procesado error en validaciones
                            }
                        }else{
                            return '103'; // telefono no puede estar en blanco
                        }
                            break;
                    case '1':
                    //estatus = 1 : tiene toda la informacion pero la actualiza
                        $actualizado = Contribuyentes::find()->where(['rif_servicio'=>$rifContribuyente])->one();
                        $actualizado->telefono = $telefono;
                        $actualizado->estatus = $actualizado->getEstatus($nombres,$apellidos,$correo);
                        if ($actualizado->telefono) { 
                            if ($actualizado->validate()) {
                                $actualizado->save();
                                if ($actualizado->save()) {
                                    return '200'; //se crea un nuevo registro para el contribuyente 
                                }else{
                                    return '102'; // ocurrio un error al guardar en la base de datos
                                }
                            }else {
                                return '101'; // no procesado error en validaciones
                            }
                        }else{
                            return '103'; // telefono no puede estar en blanco
                        }
                        break;
                    case '2':
                    //estatus = 2 : le falta toda la informacion y realiza algun cambio
                        $actualizado = Contribuyentes::find()->where(['rif_servicio'=>$rifContribuyente])->one();
                        $actualizado->telefono = $telefono;
                        $actualizado->estatus = $actualizado->getEstatus($nombres,$apellidos,$correo);
                        if ($actualizado->telefono) { 
                            if ($actualizado->validate()) {
                                $actualizado->save();
                                if ($actualizado->save()) {
                                    return '200'; //se crea un nuevo registro para el contribuyente 
                                }else{
                                    return '102'; // ocurrio un error al guardar en la base de datos
                                }
                            }else {
                                return '101'; // no procesado error en validaciones
                            }
                        }else{
                            return '103'; // telefono no puede estar en blanco
                        }
                        break;
                    case '3':
                    //estatus = 3 : le faltan dos datos
                        $actualizado = Contribuyentes::find()->where(['rif_servicio'=>$rifContribuyente])->one();
                        $actualizado->telefono = $telefono;
                        $actualizado->estatus = $actualizado->getEstatus($nombres,$apellidos,$correo);
                        if ($actualizado->telefono) { 
                            if ($actualizado->validate()) {
                                $actualizado->save();
                                if ($actualizado->save()) {
                                    return '200'; //se crea un nuevo registro para el contribuyente 
                                }else{
                                    return '102'; // ocurrio un error al guardar en la base de datos
                                }
                            }else {
                                return '101'; // no procesado error en validaciones
                            }
                        }else{
                            return '103'; // telefono no puede estar en blanco
                        }
                        break;
                    case '4':
                    //estatus = 4 : le falta al menos un dato
                        $actualizado = Contribuyentes::find()->where(['rif_servicio'=>$rifContribuyente])->one();
                        $actualizado->telefono = $telefono;
                        $actualizado->estatus = $actualizado->getEstatus($nombres,$apellidos,$correo);
                        if ($actualizado->telefono) { 
                            if ($actualizado->validate()) {
                                $actualizado->save();
                                if ($actualizado->save()) {
                                    return '200'; //se crea un nuevo registro para el contribuyente 
                                }else{
                                    return '102'; // ocurrio un error al guardar en la base de datos
                                }
                            }else {
                                return '101'; // no procesado error en validaciones
                            }
                        }else{
                            return '103'; // telefono no puede estar en blanco
                        }
                        break;
                    case '6':
                    //estatus = 6 : errores en validacion
                        return "101";
                        break;
                    default:
                    // no realizo ningun cambio
                        return "100"; 
                        break;
                }
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }
        /**
         * @param string $rif
         * @return string
         * @soap
         */
        public function getRif($rif)
        {
            if(!$rif){
                throw new Exception('el rif no puede estar en blanco');
            }
            if($rif  == null || $rif == ''){
                throw new Exception('el rif no puede ser nulo');
              }
            
            try {
            //si no hay excepciones ejecutamos el servicio 
                $rif = strtoupper($rif);
                $contribuyente = new Contribuyentes();
                $contribuyente->rif_servicio = $rif;
                if ($contribuyente->validate()) {
                //validamos que el rif no tenga caracteres especiales 
                    $rifVal = $contribuyente->rif_servicio;
                    $letra = substr($rifVal,0,1);
                    $letra = strtoupper($letra);
                    $numero = substr($rifVal,1,10);

                        $validaciones = new Validaciones();
                        $validaciones->numero = $numero;
                        $validaciones->letra = $letra;

                    if($validaciones->validate()){
                    //si el rif es valido buscamos el contribuyente en el WS
                        $datos1 = Contribuyentes::find()
                                            ->where(['rif_servicio'=>$rifVal])
                                            ->one();
                        if(!$datos1){
                        //si el rif no esta en la BD del servicio buscamos en la base de datos de la extranet
                            $datos2 = UsuariosOld::find('rif_extranet,razon_social') 
                            ->where(['rif_extranet'=>$rifVal])
                            ->one();
                            if(!$datos2){
                            //si el rif no esta en la BD de la extranet retornamos un string con status 0
                                return '0'.'||'.''.'||'.''.'||'.''.'||'.''.'||'.''.'||';
                            }else{
                                $rsExtranet = $datos2->razon_social;
                                $rsExtranet  = strtok($rsExtranet, '(');
                                $datos2->estatus = (string)2;
                                $estatus = $datos2->estatus;
                                $rifExtranet = $datos2->rif_extranet;
                                return $estatus.'||'.$rifExtranet.'||'.$rsExtranet.'||'.''.'||'.''.'||'.''.'||'.''.'||';
                            }
                            
                        }else{
                            
                            $estatus = (string)$datos1->estatus;
                            $rifServicio = $rifVal;
                            $rsServicio = $datos1->razon_social;
                            $nombres = $datos1->nombres;
                            $apellidos = $datos1->apellidos;
                            $correo = $datos1->correo;
                            $telefono = $datos1->telefono;

                            return $estatus.'||'.$rifServicio.'||'.$rsServicio.'||'.$nombres.'||'.$apellidos.'||'.$correo.'||'.$telefono.'||';
                        }

                    }else{
                        return '6'.'||'.''.'||'.''.'||'.''.'||'.''.'||'.''.'||'; // no procesado error en validaciones
                    }
                }else{
                    return '6'.'||'.''.'||'.''.'||'.''.'||'.''.'||'.''.'||'; // no procesado error en validaciones
                }
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }
     

}
