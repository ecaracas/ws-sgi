<?php

namespace app\modules\usuarios\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $id_user
 * @property string $dni_letter
 * @property string $dni_number
 * @property string $razon_social
 * @property string $d1
 * @property string $d2
 * @property string $d3
 * @property string $estatus
 */
class Usuarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni_letter', 'dni_number'], 'required'],
            [['dni_letter'], 'string', 'max' => 1],
            [['dni_number'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_user' => 'Id User',
            'dni_letter' => 'Dni Letter',
            'dni_number' => 'Dni Number',
            'razon_social' => 'Razon Social',
        ];
    }

    public function __toString()
    {
        return $this->razon_social . '||' . $this->dni_letter.'||'.$this->dni_number.'||'.$this->d1.'||'.$this->d2.'||'.$this->d3.'||'.$this->estatus;
    }
}
