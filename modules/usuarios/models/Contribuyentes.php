<?php

namespace app\modules\usuarios\models;

use Yii;

/**
 * This is the model class for table "contribuyentes".
 *
 * @property int $id_contribuyente
 * @property string $rif_servicio
 * @property string $razon_social
 * @property string $telefono
 * @property string $nombres
 * @property string $apellidos
 * @property string $correo
 * @property int $estatus
 */
class Contribuyentes extends \yii\db\ActiveRecord
{
    public $letra;
    public $numero;
    public $x1,$x2,$x3;
   // public $nuevoNombre, $nuevoApellido,$nuevoCorreo;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contribuyentes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rif_servicio'], 'required'],
            [['rif_servicio'], 'validarif'],
            [['telefono'], 'validatelefono'],
            [['telefono'], 'match', 'pattern' => '/^[0-9]/','message'=>'solo numeros!'],
            [['correo'], 'email'],
            [['nombres','apellidos'], 'string', 'max' => 50,'message'=>'debe tener un maximo de 50 caracteres'],
            [['nombres','apellidos'], 'match', 'pattern' => '/^[a-zA-Z0-9]/','message'=>'solo se aceptan letras y numeros'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_contribuyente' => 'Id Contribuyente',
            'rif_servicio' => 'Rif Servicio',
            'razon_social' => 'Razon Social',
            'telefono' => 'Telefono',
            'nombres' => 'Nombres',
            'apellidos' => 'Apellidos',
            'correo' => 'Correo',
            'estatus' => 'Estatus',
        ];
    }


    public function validaRif($attribute)
    {
        $this->$attribute = strtoupper($this->$attribute);
        $this->$attribute = str_replace('-', '', $this->$attribute);
        $this->$attribute = str_replace('.', '', $this->$attribute);
        $this->$attribute = str_replace(',', '', $this->$attribute);

        if (strlen($this->$attribute) != 10) {
            $this->addError($attribute, 'El rif debe tener una logitud de 10 digitos');
        }
    }




    public function validaLongitud($attribute) {

        if (strlen($this->$attribute) != 9) {
            $this->addError($attribute, 'El RIF debe tener una logitud de 9 digitos');
        }
    }

    public function validaTelefono($attribute)
    {

        $this->$attribute = str_replace('_', '', $this->$attribute);
        $this->$attribute = str_replace('-', '', $this->$attribute);
        $this->$attribute = str_replace('/', '', $this->$attribute);
        $this->$attribute = str_replace('(', '', $this->$attribute);
        $this->$attribute = str_replace(')', '', $this->$attribute);

        if (strlen($this->$attribute) != 11) {
            $this->addError($attribute, 'El telefono debe tener una logitud de 11 digitos');
        }
    }

    

    public function getEstatus($nombres,$apellidos,$correo)
    {
        if ($this->nombres != $nombres || $this->apellidos != $apellidos || $this->correo != $correo ) {
            $this->nombres = $nombres;
            $this->apellidos = $apellidos;
            $this->correo = $correo;
        }
       

        $x1 = ($this->nombres != null) ? 1 : 0 ;
        $x2 = ($this->apellidos != null) ? 1 : 0 ;
        $x3 = ($this->correo != null) ? 1 : 0 ;
        $sum = ($x1 + $x2 + $x3);

        
        switch ($sum) {
            case '0':
                $this->estatus=2; //no tiene informacion
                break;
            case '1':
                $this->estatus=3; //le faltan dos datos
                break;
            case '2':
                $this->estatus=4; // le falta un dato
                break;
            case '3':
                $this->estatus=1; // tiene todos los datos
                break;
            default:
                $this->estatus = 0; // por defecto indicamos que esta vacio
                break;
        }
        
        return $this->estatus;
    }
}
