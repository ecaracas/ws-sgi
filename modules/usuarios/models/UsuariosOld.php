<?php

namespace app\modules\usuarios\models;

use Yii;

/**
 * This is the model class for table "usuarios_old".
 *
 * @property int $id_usuario_old
 * @property string $rif_extranet
 * @property string $razon_social
 */
class UsuariosOld extends \yii\db\ActiveRecord
{
    public $estatus;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios_old';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db2');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rif_extranet', 'razon_social'], 'required'],
            [['rif_extranet'], 'string', 'max' => 10],
            [['razon_social'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_usuario_old' => 'Id Usuario Old',
            'rif_extranet' => 'Rif Extranet',
            'razon_social' => 'Razon Social',
        ];
    }
}
