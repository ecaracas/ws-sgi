<?php

namespace app\modules\usuarios\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Validaciones extends Model
{
    public $letra;
    public $numero;
    public $telefono;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['numero'], 'match', 'pattern' => '/^[0-9]/','message'=>'solo numeros!'],
            ['letra', 'in', 'range' => ['V','E','G','J'],'message'=>'la letra debe ser valida'],
            [['telefono'],'validatelefono'],
        ];
    }

    public function validaTelefono($attribute)
    {

        $this->$attribute = str_replace('_', '', $this->$attribute);

        if (strlen($this->$attribute) != 11) {
            $this->addError($attribute, 'El telefono debe tener una logitud de 11 digitos');
        }
    }

    

   
}
