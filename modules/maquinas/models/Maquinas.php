<?php

namespace app\modules\maquinas\models;

use Yii;

/**
 * This is the model class for table "maquinas".
 *
 * @property int $id_machine
 * @property string $serial
 * @property string $marca
 * @property string $modelo
 */
class Maquinas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'maquinas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['serial', 'marca', 'modelo'], 'required'],
            [['serial', 'marca', 'modelo'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_machine' => 'Id Machine',
            'serial' => 'Serial',
            'marca' => 'Marca',
            'modelo' => 'Modelo',
        ];
    }
}
