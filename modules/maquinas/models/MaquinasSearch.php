<?php

namespace app\modules\maquinas\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\maquinas\models\Maquinas;

/**
 * MaquinasSearch represents the model behind the search form of `app\modules\maquinas\models\Maquinas`.
 */
class MaquinasSearch extends Maquinas
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_machine'], 'integer'],
            [['serial', 'marca', 'modelo'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Maquinas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_machine' => $this->id_machine,
        ]);

        $query->andFilterWhere(['like', 'serial', $this->serial])
            ->andFilterWhere(['like', 'marca', $this->marca])
            ->andFilterWhere(['like', 'modelo', $this->modelo]);

        return $dataProvider;
    }
}
