<?php

namespace app\modules\cliente\controllers;

use yii\web\Controller;

/**
 * Default controller for the `cliente` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        echo "hola desde el index del cliente";exit;
        return $this->render('index');
    }
}
