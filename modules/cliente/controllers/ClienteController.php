<?php

namespace app\modules\cliente\controllers;

use mongosoft\soapclient\Client;
use yii\console\Exception;

class ClienteController extends \yii\web\Controller
{
    
    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionRif()
    {
        $client = new Client([
            'url' => 'http://localhost/WSYii2Soap/web/servicios/server/rif',
        ]);

     
        echo $client->getRif('v',200959627);
    }

    public function actionSumar()
    {
        $client = new Client([
            'url' => 'http://localhost/WSYii2Soap/web/servicios/server/rif',
        ]);
        
        $n1 = 1;
        $n2 = 3;
        $resultado = $client->getSuma($n1,$n2);
        
        echo $resultado;exit;
    }

    public function actionTest()
    {
        try {
            
            $client = new Client([
                'url' => 'http://localhost/WSYii2Soap/web/usuarios/usuarios/getrif?wsdl',
            ]);
            
            
            
            echo $client->getRif('v',123456789);
            
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function actionSet()
    {
        //parametros de busqueda
        $rif = '123456789';
        $letra = 'v';

        //parametros

        $telf = '6546546789';
        $email = 't@m.com';
        $direccion = '';

        if(!$rif || !$letra){
            throw new Exception('falta uno de los parametros');
          }

        try {
            $client = new Client([
                'url' => 'http://localhost/WSYii2Soap/web/usuarios/usuarios/setrif?wsdl',
            ]);
            echo $client->setRif($letra,$rif,$telf,$email,$direccion);
            
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }



    public function actionContribuyentes()
    {
        $letra_rif = 'v';
        $numero_rif = 77520860;

        
        
        $client = new Client([
            'url' => 'http://localhost/WSYii2Soap/web/usuarios/usuarios/servicios?wsdl',
        ]);

        $contribuyente = $client->getContribuyentes($letra_rif,$numero_rif);

        return $this->render('index',['contribuyente'=>$contribuyente]);
    }

    public function actionCrea()
    {
        //parametros obligatorios
        $letra_rif = 'v';
        $numero_rif = 200959627;
        $razon_social = '';

        //parametros de actualizacion
        $telefono ='12345678978';
        $nombres = 'elias';
        $apellidos = 'caracas';
        $correo = '';
        $estatus = '';

        // print_r($razon_social.",".$letra_rif.",".$numero_rif);die;

        $client = new Client([
            'url' => 'http://localhost/WSYii2Soap/web/usuarios/usuarios/servicios?wsdl',
        ]);

        $contribuyente = $client->setContribuyentes($letra_rif,$numero_rif,$telefono,$nombres,$apellidos,$correo,$razon_social);

        return $this->render('crear',['contribuyente'=>$contribuyente]);
    }

}
