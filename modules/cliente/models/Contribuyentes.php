<?php

namespace app\modules\cliente\models;

use Yii;

/**
 * This is the model class for table "contribuyentes".
 *
 * @property int $id_contribuyente
 * @property string $rif_servicio
 * @property string $razon_social
 * @property string $telefono
 * @property string $nombres
 * @property string $apellidos
 * @property string $correo
 * @property int $estatus
 */
class Contribuyentes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contribuyentes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rif_servicio', 'razon_social'], 'required'],
            [['telefono', 'estatus'], 'integer'],
            [['rif_servicio'], 'string', 'max' => 10],
            [['razon_social', 'correo'], 'string', 'max' => 150],
            [['nombres', 'apellidos'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_contribuyente' => 'Id Contribuyente',
            'rif_servicio' => 'Rif Servicio',
            'razon_social' => 'Razon Social',
            'telefono' => 'Telefono',
            'nombres' => 'Nombres',
            'apellidos' => 'Apellidos',
            'correo' => 'Correo',
            'estatus' => 'Estatus',
        ];
    }
}
