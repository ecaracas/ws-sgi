<?php

namespace app\modules\servicios\controllers;

use app\modules\servicios\models\RbacUsuarios;
use app\modules\usuarios\Usuarios;


class ServerController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actions()
    {
        return [
            'rif' => 'mongosoft\soapserver\Action',
            'suma' => 'mongosoft\soapserver\Action',
        ];
    }
    /**
     * @param string $rif
     * @return string
     * @soap
     */
    public function getRif($letra,$rif)
    {
        if(!$rif || !$letra){
            throw new Exception('falta uno de los parametros');
          }
          
      try {          
          $model = new RbacUsuarios(); //instanciamos el modelo donde buscaremos los datos
          $model->dni_letra=$letra; //asociamos los atributos del modelo a los parametro del metodo
          $model->dni_numero=$rif;
          
          if ($model->validate()) { //se realizan las validaciones del lado del servicio
              
              $data = RbacUsuarios::find('nombre,dni_letra,dni_numero')
                                      ->where(['dni_letra'=>$letra])
                                      ->andWhere(['dni_numero'=>$rif])
                                      ->one();

                                      if (!$data) {
                                          return 'No se encontraron resultados';
                                      }else{
                                          return (string)$data;  //devolvemos un string con la informacion obtenida
                                      }

          }else{
              $errores  ='';
              foreach($model->errors as $key => $value){
                  $llave = $key;
                  foreach($value as $key => $value){
                      $errores .= $llave.": ".$value.", ";
                  }
              }
              return $errores;
          }
          
      } catch (Exception $e) {

          return $e->getMessage();

      }

    }
    /**
     * @param int $num1
     * @param int $num2
     * @param int $result
     * @return int
     * @soap
     */
    public function getSuma($num1,$num2)  
    {
        $result = $num1 + $num2;
        
        return $result;
    }


    

}
