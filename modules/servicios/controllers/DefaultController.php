<?php

namespace app\modules\servicios\controllers;

use yii\web\Controller;

/**
 * Default controller for the `servicios` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    // http://localhost/WSYii2Soap/web/servicios/default/index
    
    public function actionIndex()
    {
        echo "hola desde el index del servicio";exit;
        return $this->render('index');
    }

    // http://localhost/WSYii2Soap/web/servicios/default/hola

    public function actionHola()
    {
        echo "hola desde el hola del servicio";exit;
        return $this->render('index');
    }
}
