<?php

namespace app\modules\servicios\models;

use Yii;

/**
 * This is the model class for table "rbac_usuarios".
 *
 * @property string $id_rbac_usuario
 * @property string $id_rbac_rol
 * @property string $nombreusuario
 * @property string $email
 * @property string $nombre
 * @property string $apellido
 * @property string $estatus
 * @property string $dni_numero
 * @property string $telf_local_numero
 * @property string $cambio_clave
 * @property string $lastlogin_time
 * @property string $dni_letra
 * @property string $fecha_creado
 * @property string $fecha_editado
 * @property string $fecha_eliminado
 * @property string $creado_por
 * @property string $editado_por
 * @property string $eliminado_por
 * @property string $contrasena
 * @property string $access_token
 * @property string $authkey
 * @property string $verification_code
 *
 * @property RbacRolesOpcionesUsuarios[] $rbacRolesOpcionesUsuarios
 * @property RbacRoles $rbacRol
 */
class RbacUsuarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rbac_usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni_numero'], 'required'],
            [['id_rbac_rol'], 'integer'],
            [['dni_letra'], 'string'],
            [['dni_numero'], 'string', 'max' => 9,'tooLong'=>'el rif puede tener máximo 9 digitos'],
            [['dni_numero'], 'match', 'pattern' => '/^[0-9a-zA-Z]+$/','message'=>'solo se permiten numeros']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_rbac_usuario' => 'Id Rbac Usuario',
            'id_rbac_rol' => 'Id Rbac Rol',
            'nombreusuario' => 'Nombreusuario',
            'email' => 'Email',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'estatus' => 'Estatus',
            'dni_numero' => 'Dni Numero',
            'telf_local_numero' => 'Telf Local Numero',
            'cambio_clave' => 'Cambio Clave',
            'lastlogin_time' => 'Lastlogin Time',
            'dni_letra' => 'Dni Letra',
            'fecha_creado' => 'Fecha Creado',
            'fecha_editado' => 'Fecha Editado',
            'fecha_eliminado' => 'Fecha Eliminado',
            'creado_por' => 'Creado Por',
            'editado_por' => 'Editado Por',
            'eliminado_por' => 'Eliminado Por',
            'contrasena' => 'Contrasena',
            'access_token' => 'Access Token',
            'authkey' => 'Authkey',
            'verification_code' => 'Verification Code',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRbacRolesOpcionesUsuarios()
    {
        return $this->hasMany(RbacRolesOpcionesUsuarios::className(), ['id_rbac_usuario' => 'id_rbac_usuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRbacRol()
    {
        return $this->hasOne(RbacRoles::className(), ['id_rbac_rol' => 'id_rbac_rol']);
    }

    public function __toString()
    {
        return $this->dni_letra . '||' . $this->dni_numero.'||'.$this->nombre;
    }
}
